module main(
	input Clock,
	input [15:0]DIN,
	input Reset, //Reinicia o sistema.
	output ADDRin,  //Define o endereço de saída para escrita na memória.
	output [15:0] ADDR, 	//Endereço de escrita do dado.
	output DOUTin,  //Define o dado de saída  para escrita na memória..
	output [15:0] DOUT,
	output W_D,     //Habilita a escrita na memória.
	output reg[15:0] BusWires,
	output Done,     //Informa o fim da instrução.
	input [15:0]ramlmp_output,
	output [15:0]R0_output,
	output RAMout,
	output reg [10:0]MUXop
); 
	wire [2:0]Counter;
	wire [15:0]A_output, W_output, RNin;
	wire [9:0]IR_output;
	wire [10:0]IRin;
	wire incr_pc;
	wire [7:0]RNout;
	wire Ain, Gin, Gout, DINout; 
	wire [2:0]ALUop;
	//Saída dos registradores R0 a R7.
	wire [15:0] G_output, ALU_output, R1_output, R2_output, R3_output, R4_output, R5_output, R6_output, R7_output;
	//reg [10:0]MUXop;
	 
	wire Clear = Done | Reset;
  
    //Unidade Counter
	Counter C(Clock, Clear, Counter);
    
	wire [7:0]XXX, YYY;
	RegUpcode X(DIN[5:3], XXX);
    RegUpcode Y(DIN[2:0], YYY); 
   
	ControlUnit CU(IR_output, Counter, XXX, YYY, G_output, IRin, RNout, RNin, incr_pc, Ain, Gin, Gout, DINout, ALUop, ADDRin, RAMout, DOUTin, W_D, Done);
	 	  
	ALU alu(A_output, BusWires, ALUop, ALU_output);
	 	 
    //Registradores
	RegN G(Clock, Gin, ALU_output, G_output);
	RegN R0(Clock, RNin[0], BusWires, R0_output);
	RegN R1(Clock, RNin[1], BusWires, R1_output);
	RegN R2(Clock, RNin[2], BusWires, R2_output);
	RegN R3(Clock, RNin[3], BusWires, R3_output);
	RegN R4(Clock, RNin[4], BusWires, R4_output);
	RegN R5(Clock, RNin[5], BusWires, R5_output);
	RegN R6(Clock, RNin[6], BusWires, R6_output);
	RegN A(Clock, Ain, BusWires, A_output);
	//RegPC R7(Clock, incr_pc, BusWires, RNin[7], Reset, R7_output);

	RegIR IR(Clock, IRin, DIN, IR_output);
     
	 
   // Multiplexers M(MUXop, DIN, RNout, Gout, DINout, G_output, R0_output, R1_output,R2_output, R3_output, R4_output, R5_output, R6_output, R7_output, BusWires);
	always @(MUXop, RNout, Gout, DINout, RAMout) begin
        MUXop[9:2] = RNout[7:0];
        MUXop[1] = Gout;
        MUXop[0] = DINout;
        MUXop[10] = RAMout;
        
        case(MUXop)
            11'b00000000001: BusWires <= DIN;
            11'b00000000010: BusWires <= G_output;
            11'b00000000100: BusWires <= R0_output;
            11'b00000001000: BusWires <= R1_output;
            11'b00000010000: BusWires <= R2_output;
            11'b00000100000: BusWires <= R3_output;
            11'b00001000000: BusWires <= R4_output;
            11'b00010000000: BusWires <= R5_output;
            11'b00100000000: BusWires <= R6_output;
            11'b10000000000: BusWires <= ramlmp_output;
            //10'b1000000000: BusWires = R7_output;
        endcase       
    end
      
    RegN reg_ADDR(Clock, ADDRin, BusWires, ADDR);
    RegN reg_DOUT(Clock, DOUTin, BusWires, DOUT);
    //RegW W(Clock, W_D, BusWires, W_output);
    
endmodule

//Modulo que converte um número binário de 3 bits para um número decimal.
module RegUpcode(	//Pode esta errado agora
   input [2:0] reg_input,
   output reg[7:0]reg_output
);

	always @(reg_input) begin
		case (reg_input)
			3'b000: reg_output = 8'b00000001;
			3'b001: reg_output = 8'b00000010;
			3'b010: reg_output = 8'b00000100;
			3'b011: reg_output = 8'b00001000;
			3'b100: reg_output = 8'b00010000;
			3'b101: reg_output = 8'b00100000;
			3'b110: reg_output = 8'b01000000;
			3'b111: reg_output = 8'b10000000;
		endcase
	end
endmodule
/*
module mainTest();
  reg clk, Reset;
  reg [15:0]DIN;
  wire [15:0] ramlmp_output;
  wire ADDRin, DOUTin, W_D, Done; //Define o endereço de saída para escrita na memória.
  wire [15:0] ADDR, DOUT, BusWires;  	//Endereço de escrita do dado.
 
   
    always
      clk <= ~clk;
      

    initial    begin
		    clk <= 0;
    #1 DIN <=16'b0000_00_0000_000_000;
    #1 DIN <=16'b0000_00_0000_000_000;
    #1 DIN <=16'b0000_00_0000_000_000;
		#1 DIN <=16'b0000_00_0000_000_000;
	  #1 DIN <=16'b0000_00_0000_000_000;
		    #4 $stop;
	   end

 
    main mamamoo(Clock, DIN, Reset, ADDRin, ADDR, DOUTin, DOUT, W_D, BusWires, Done, ramlmp_output);
    
    initial begin
        $monitor("Clk %d  Done %b  DIN %b ramlmp_output %b\n ADDR %b DOUT %b", clk, Done, DIN, ramlmp_output, ADDR, DOUT );
    end  

endmodule

    
    
    
//endmodule
*/
