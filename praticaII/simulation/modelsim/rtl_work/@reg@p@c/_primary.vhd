library verilog;
use verilog.vl_types.all;
entity RegPC is
    port(
        Clock           : in     vl_logic;
        incr_pc         : in     vl_logic;
        \Bus\           : in     vl_logic_vector(15 downto 0);
        Rin             : in     vl_logic;
        Reset           : in     vl_logic;
        R_output        : out    vl_logic_vector(10 downto 0)
    );
end RegPC;
