library verilog;
use verilog.vl_types.all;
entity display_7seg is
    port(
        data            : in     vl_logic_vector(15 downto 0);
        ML_HEXO         : out    vl_logic_vector(6 downto 0);
        ML_HEX1         : out    vl_logic_vector(6 downto 0);
        ML_HEX2         : out    vl_logic_vector(6 downto 0);
        ML_HEX3         : out    vl_logic_vector(6 downto 0)
    );
end display_7seg;
