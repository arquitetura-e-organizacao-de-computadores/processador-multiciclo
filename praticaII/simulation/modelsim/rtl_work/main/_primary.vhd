library verilog;
use verilog.vl_types.all;
entity main is
    port(
        Clock           : in     vl_logic;
        DIN             : in     vl_logic_vector(15 downto 0);
        Reset           : in     vl_logic;
        ADDRin          : out    vl_logic;
        ADDR            : out    vl_logic_vector(15 downto 0);
        DOUTin          : out    vl_logic;
        DOUT            : out    vl_logic_vector(15 downto 0);
        W_D             : out    vl_logic;
        BusWires        : out    vl_logic_vector(15 downto 0);
        Done            : out    vl_logic;
        ramlmp_output   : in     vl_logic_vector(15 downto 0);
        R0_output       : out    vl_logic_vector(15 downto 0);
        RAMout          : out    vl_logic;
        MUXop           : out    vl_logic_vector(10 downto 0)
    );
end main;
