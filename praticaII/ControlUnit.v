module ControlUnit(
	input [9:0]IR,          //Define a instrução executada.
	input [2:0]Counter,     //Contador para que não ocorra sobreposição de instruções.
	input [7:0]XXX,         //Define o reg XXX da instrução.
	input [7:0]YYY,         //Define o reg XXX da instrução.    
	input [15:0] G_output,         //Define a saída de G.    
	output reg [10:0]IRin,  //Habilita a escrita em IR.    
	output reg [7:0]RNout,  //Define se a saída dos reg R0 a R7 seram utilizados.
	output reg [7:0]RNin,   //Habilita a escrita de dados nos reg R0 a R7.
	output reg incr_pc,     //Habilita a escrita no reg R7: incremento do PC.    
	output reg Ain,         //Habilita o uso do registrador A. 
	output reg Gin,         //Habilita o uso do registrador G.
	output reg Gout,
	output reg DINout,      //Define se a próxima IR será uma instrução ou dado da chamada imediata.    
	output reg[2:0]ALUOp,   //Define a operação da ULA.    
	output reg ADDRin,      //Habilita o reg ADDR, que armazena o endereço utilizado na memória.
	output reg RAMout,
	output reg DOUTin,      //Habilita o reg DOUT, que armazena o dado utilizado na memória.
	output reg W_D,         //Habilita a escrita na memória.
	output reg Done     	 //Informa o término da instrução.
);  
    
	wire[3:0] ADD = 4'b0000, SUB = 4'b0001, AND = 4'b0010, SLT = 4'b0011, SLL = 4'b0100, SRL = 4'b0101, LD = 4'b0110, ST = 4'b0111, MVNZ = 4'b1000, MV = 4'b1001, MVI = 4'b1010; 
 // ControlUnit CU(10 IR_output, 2 Counter, 7 XXX, 7 YYY, G_output, 10 IRin, 7 RNout, 7 RNin, incr_pc, Ain, Gin, DINout, 2 ALUop, ADDRin, DOUTin, W_D, Done);

	always @(Counter or IR or XXX or YYY) begin
        //Especificação de valores para todo início de execução de instrução.
	 IRin = 1'b0;
	 RNout[7:0] = 8'b00000000;
	 RNin[7:0] = 8'b00000000;
	 DINout = 1'b0;
	 Ain = 1'b0;
	 Gin = 1'b0;
	 Gout = 1'b0;
	 DOUTin = 1'b0;
	 ADDRin = 1'b0;
	 W_D = 1'b0;
	 incr_pc = 1'b0;
	 Done = 1'b0;
	 RAMout = 1'b0;
	 
		case (Counter)
			3'b000: begin //Se o contador for igual a zero, ocorrerá a busca de uma nova instrução.
				ADDRin = 1'b1;  //Habilita a leitura na memória de instruções.
				// incr_pc = 1'b1; //Habilita a escrita em R7.
			end
			3'b001: begin
				IRin = 1'b1;    //Habilita a escrita em IR.
				//ADDRin = 1'b1;  //Habilita a leitura na memória de instruções.
			end
			3'b010: begin
				case (IR[9:6]) 
					ADD, SUB, AND, SLT: begin  //Instrução add, sub, and, slt.
						RNout = XXX;    //Define o registrador de leitura XXX.
						Ain = 1'b1;     //Habilita a escrita no reg A.
					end
					SLL: begin //Instrução sll.
						RNout = XXX;    //Define o registrador de leitura XXX.
						Gin = 1'b1;     //Habilita a escrita no reg A.
						ALUOp = 3'b011; //Define a operação de sll na ULA.
					end
					SRL: begin  //Instrução srl.
						RNout = XXX;    //Define o registrador de leitura XXX.
						Gin = 1'b1;     //Habilita a escrita no reg A.
						ALUOp = 3'b111; //Define a operação de srl na ULA.
					end
					LD: begin //Instrução ld.
						RNout = YYY;    //Define o registrador de leitura YYY.
						ADDRin = 1'b1;  //Habilita a escrita em ADDR.
					end
					ST: begin  //Instrução st.
						RNout = XXX;    //Define o registrador de leitura XXX.
						DOUTin = 1'b1;  //Habilita e escrita em DOUT.
					end
                  MVNZ: begin //Instrução mvnz.
                        //Se o dado em G for diferente de 0, move-se o conteúdo de YYY  para XXX.
                        if(G_output) begin
                            RNout = YYY;
                            RNin = XXX;    //Habilita a escrita no reg XXX.
                        end                        
                    end
                    MV: begin //Instrução mv: XXX recebe o dado contido em YYY.
                        RNout = YYY;
                        RNin = XXX;     //Habilita a escrita no reg XXX.
                    end
                    MVI: begin //Instrução mvi.
								RNin = XXX;     //Habilita a escrita no reg XXX.
								DINout = 1'b1;
                    end				  
				endcase
			end
			3'b011: begin
				case (IR[9:6])
					ADD: begin  //Instrução add.
						RNout = YYY;    //Define o registrador de leitura YYY.
						Gin = 1'b1;     //Habilita a escrita no reg G.
						ALUOp = 3'b000; //Define a operação de add na ULA.						
					end 
					SUB: begin   //Instrução sub.
						RNout = YYY;    //Define o registrador de leitura YYY.
						Gin = 1'b1;     //Habilita a escrita no reg G.
						ALUOp = 3'b001; //Define a operação de sub na ULA.
					end
					AND: begin  //Instrução and.
						RNout = YYY;    //Define o registrador de leitura YYY.
						Gin = 1'b1;     //Habilita a escrita no reg G.
						ALUOp = 3'b010; //Define a operação de and na ULA.
					end
					SLT: begin  //Instrução slt.
						RNout = YYY;    //Define o registrador de leitura YYY.
						Gin = 1'b1;     //Habilita a escrita no reg G.
						ALUOp = 3'b101; //Define a operação de slt na ULA.
					end
					SLL, SRL: begin //Instrução sll e srl.
						Gout = 1'b1;    //Define que o dado de escrita é da saída da ULA.
						RNin = XXX;     //Habilita a escrita no reg XXX.
               end
					LD: begin      //Instrução ld.
						RNin = XXX;    
						RAMout = 1'b1;							  
					end
					ST: begin      //Instrução st.
						RNout = YYY;    //Define o registrador de leitura YYY.
						ADDRin = 1'b1;  //Habilita e escrita em DOUT.
					end
					MV, MVNZ: begin
						Done = 1'b1;    //Define o término da instrução.
					end
				endcase
			end
			3'b100: begin		
				case (IR[9:6]) 
					ADD, SUB, AND, SLT: begin    //Instrução add, sub, and, slt.
						Gout = 1'b1;    //Define que o dado de escrita é da saída da ULA.
						RNin = XXX;     //Habilita a escrita no reg XXX.
						Done = 1'b1;    //Define o término da instrução.
					end
					SRL, SLL: begin						
						Done = 1'b1;    //Define o término da instrução.
					end
					LD: begin
					end
					ST: begin
						W_D = 1'b1;     //Habilita e escrita na memória.
					end
					MVI: begin
						Done = 1'b1;    //Define o término da instrução.
					end
				endcase
			end
				3'b101: begin
					 case (IR[9:6]) 
                //ADD, SUB, AND, SLT: begin  //Instrução add, sub, and, slt.
					 //	  Done = 1'b1;    //Define o término da instrução.
					 //	       end
					ST: begin
							Done = 1'b1;    //Define o término da instrução.
						 end
					 LD: begin	
							Done = 1'b1;    //Define o término da instrução.
					 end
					  endcase
				end
        endcase        
    end
endmodule


