module RegN(
    input Clock,
    input Rin,  //Habilita a escrita de dados no reg RN.
    input [15:0]Bus,    //Dado de escrita em RN.
    output reg[15:0]R_output  //Dado de saída de RN.
);  
	initial begin
		R_output = 16'b0000_0000_0000_0001;
	end
	
	always @(posedge Clock) begin
			if(Rin) begin
				R_output = Bus;
			end
    end
endmodule

module RegIR(
    input Clock,
    input Rin, 						//Habilita a escrita de dados no reg IR.
    input [15:0]Bus,    			//Dado de escrita em IR.
    output reg [9:0]R_output   	//Dado de saída de IR.
);
    always @(posedge Clock) begin
        if(Rin) begin
				R_output = Bus[9:0];
			end
    end
endmodule

module RegW(
    input Clock,
    input Rin,  						//Habilita a escrita de dados no reg W.
    input [15:0]Bus,   				//Dado de escrita em W.
    output reg [10:0]R_output   	//Dado de saída de W.
);
    always @(posedge Clock) begin
        if(Rin)
            R_output = Bus[10:0];
    end
endmodule

module RegPC(
    input Clock,
	 input incr_pc,				//Habilita a escrita de dados no reg W.
	 input [15:0]Bus,   			//Dado de escrita em W.
	 input Rin,  					//Habilita a leitura de dados no reg W.
	 input Reset,
    output reg [10:0]R_output //Dado de saída de W.
);
    always @(posedge Clock) begin
        if(incr_pc & ~Reset)
				R_output = R_output + 1;
		  else if(Reset)
				R_output = 0;
		  else
            R_output = Bus[10:0];
    end
endmodule
