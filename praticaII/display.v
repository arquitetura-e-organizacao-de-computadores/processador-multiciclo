module hexadecimal(ml_sig_dig, D_HEXO );
	input [6:0] ml_sig_dig;
	output reg [6:0] D_HEXO;
	
	always @(ml_sig_dig) begin
		 case(ml_sig_dig)
			0:		D_HEXO <= 7'b0000001;	//0
			1:		D_HEXO <= 7'b1001111;	//1
			2:		D_HEXO <= 7'b0010010;	//2
			3:		D_HEXO <= 7'b0000110;	//3
			4:		D_HEXO <= 7'b1001100;	//4
			5:		D_HEXO <= 7'b0100100;	//5
			6:		D_HEXO <= 7'b0100000;	//6
			7:		D_HEXO <= 7'b0001111;	//7
			8:		D_HEXO <= 7'b0000000;	//8
			9:		D_HEXO <= 7'b0001100;	//9
			10:	D_HEXO <= 7'b0001000;	//A	10
			11:	D_HEXO <= 7'b1100000;	//B	11
			12:	D_HEXO <= 7'b0110001;	//C	12
			13:	D_HEXO <= 7'b1000010;	//D	13
			14:	D_HEXO <= 7'b0110000;	//E	14
			15:	D_HEXO <= 7'b0111000;	//F	15
			default D_HEXO <= 7'b1111111;
		 endcase
	end
endmodule

module display_7seg(
	input [15:0] data,
	output reg [6:0] ML_HEXO,
	output reg [6:0] ML_HEX1,
	output reg [6:0] ML_HEX2,
	output reg [6:0] ML_HEX3
);

	always@(data)begin
		ML_HEXO = data%16;
		ML_HEX1 = (data/16)%16;
		ML_HEX2 = ((data/16)/16)%16;
		ML_HEX3 = (((data/16)/16)/16)%16;
	end
endmodule
