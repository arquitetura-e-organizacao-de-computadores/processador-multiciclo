module Counter(
    input Clock,
    input Clear,            //Habilita a execução do Counter.
    output reg [2:0]Counter //Contador para que não ocorra sobreposição de instruções.
);
    always @(posedge Clock) begin   //Em cada ciclo de clock:
        if(Clear)   //Se o sistema for reiniciado ou atingir o número máximo de ciclos de clock Counter recebe 0.
            Counter <= 3'b000;
        else    //Caso não, incrementa Counter em 1.
            Counter <= Counter + 3'b001;
    end
endmodule
