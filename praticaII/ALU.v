//ULA OK depois dos testes
module ALU(
    input [15:0]A,
    input [15:0]Bus, 
    input [2:0] ALUop,
    output reg [15:0]ALU_output
);
    always @(ALUop, Bus, A) begin
        case(ALUop)
            3'b000: ALU_output <= A + Bus;      //Add.
            3'b001: ALU_output <= A - Bus;      //Sub.
            3'b010: ALU_output <= (A & Bus);     //And.
            3'b101: ALU_output <= A < Bus ? 1 : 0;
            3'b011: ALU_output <= Bus << 1;    //Shift left.
            3'b111: ALU_output <= Bus >> 1;    //Shift right.
        endcase    
    end
endmodule

/*
module tAlu();
 reg [15:0] in1, in2;//  = 16'b0000000000000001, Bua = 16'b0000000000000001;
 wire [15:0] out ;// = 16'b0000_0000_0000_0001;
 reg [2:0] opA;
 initial    begin
		   // clk <= 0;
		   #1 opA <= 3'b101; in1 <= 1; in2 <= 1;
		   #1 opA <= 3'b101; in1 <= 3; in2 <=1;
		   #1 opA <= 3'b101; in1 <= 4'b0000; in2 <=4'b0001;
		   #1 opA <= 3'b101; in1 <= 4'b0010; in2 <=4'b0001;
		   #1 opA <= 3'b01; in1 <= 4'b0000; in2 <=4'b0001;
		   #1 opA <= 2'b10; in1 <= 4'b0100; in2 <=4'b0100;
		   #1 opA <= 2'b10; in1 <= 4'b0110; in2 <=4'b0110;
		   #1 opA <= 2'b10; in1 <= 4'b0001; in2 <=4'b0001;

		   
		    #4 $stop;
	   end
  
 ALU lulu(in1, in2, opA, out);
 initial begin
    $monitor("in1 %b in2 %b out %b", in1, in2, out);
 end  
 
 endmodule
 */