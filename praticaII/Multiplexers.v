//Nao estamos usando

/*module Multiplexers(
    output reg[9:0]MUXop,   //Define a operação do MUX
    input DIN,  //Define a instrução executada ou o dado imediato.
    
    input [7:0]RNout,   //Define qual saída de reg R0 a R7 será utilizada.
    input Gout,
    
    input DINout,  //Define se a próxima IR será uma instrução ou dado da chamada imediata.
    
	 input [15:0]G_output, //Saída do registrador G.
    input [15:0]R0_output,   //Dado da saída do reg R0.
    input [15:0]R1_output,   //Dado da saída do reg R1.
    input [15:0]R2_output,   //Dado da saída do reg R2.
    input [15:0]R3_output,   //Dado da saída do reg R3.
    input [15:0]R4_output,   //Dado da saída do reg R4.
    input [15:0]R5_output,   //Dado da saída do reg R5.
    input [15:0]R6_output,   //Dado da saída do reg R6.
    input [15:0]R7_output,   //Dado da saída do reg R7.
    
    output reg BusWires //Saída do Multiplexers.
);
    always @(MUXop or RNout or G_output or  DINout) begin
        MUXop[9:2] = RNout[7:0];
        MUXop[1] = Gout;
        MUXop[0] = DINout;
        
        case(MUXop)
            10'b0000000001: BusWires = DIN;
            10'b0000000010: BusWires = G_output;
            10'b0000000100: BusWires = R0_output;
            10'b0000001000: BusWires = R1_output;
            10'b0000010000: BusWires = R2_output;
            10'b0000100000: BusWires = R3_output;
            10'b0001000000: BusWires = R4_output;
            10'b0010000000: BusWires = R5_output;
            10'b0100000000: BusWires = R6_output;
            //10'b1000000000: BusWires = R7_output;
				//default: BusWires = 16'b000000000000000000;
        endcase       
    end
endmodule
*/